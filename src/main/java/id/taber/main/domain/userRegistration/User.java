package id.taber.main.domain.userRegistration;

import id.taber.main.domain.userRegistration.base.BaseUser;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name = "core_user")
public class User extends BaseUser<User>{
    final String prefix = "";

    @Column(name=prefix + "fullname")
    private String full_name;

    @Column(name=prefix + "email")
    private String email;

    @Column(name=prefix + "phonenumber")
    private String phone_number;

    @Column(name=prefix + "gender")
    private int gender;

    @Column(name=prefix + "type")
    private int type;
    
    // Not used, tapi terpaksa taruh karna spring butuh default constructor
    public User(){

    }

    public User(
        String full_name,
        String email,
        String phone_number,
        String pin,
        int gender
    ){
        generateUUID();
        this.full_name = full_name;
        this.email = email;
        this.phone_number = phone_number;
        this.gender = gender;
        this.setPin(pin);
    }

    public String getFullName(){
        return full_name;
    }

}