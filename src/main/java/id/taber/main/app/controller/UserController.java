// Notes, will change this code, cuz gw rasa Controller gk berhak ada di DDD

package id.taber.main.app.controller;

import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import id.taber.main.domain.userRegistration.User;
import id.taber.main.infra.repository.UserRepository;

@Controller
@RequestMapping(path="/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @PostMapping(path="/create")
    public @ResponseBody String registerUser(
        @RequestParam String full_name,
        @RequestParam String email,
        @RequestParam String phone_number,
        @RequestParam Integer gender,
        @RequestParam String pin
    ) throws NoSuchAlgorithmException {
        // Ini harusnya bisa di solve pake factory method, but still unsure
        User user = new User(full_name, email, phone_number, pin, gender);
        userRepository.save(user);
        return "Success";
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<User> getAllUsers() {
		return userRepository.findAll();
	}

}