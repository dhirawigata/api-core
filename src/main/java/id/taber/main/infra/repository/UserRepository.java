package id.taber.main.infra.repository;

import org.springframework.data.repository.CrudRepository;

import id.taber.main.domain.userRegistration.User;

public interface UserRepository extends CrudRepository<User, String> { }