# api-core

> :mega: Gak baca README = Mandul

## How to start
Bikin user baru untuk DB
Bikin DB kosong di Postgresql
CREATE DATABASE taber_core;

set spring.jpa.hibernate.ddl-auto ke create jika lu baru mulai start bikin project
set spring.jpa.hibernate.ddl-auto=create ke update kalo lu dah start

spring.datasource.url=jdbc:postgresql://localhost:5432/{nama_db}
spring.datasource.username={user_db}
spring.datasource.password={password_user_db}


Jalanin project pake
./gradlew bootRun for Linux user
gradlew.bat for Windows User

## How to create user
Ke Postman, hit localhost:8080/user/create dengan url params:
- full_name
- email
- phone_number
- gender
- pin
Otw bakal ganti url params ke body soon

## How to check user created
Ke Postman, hit localhost:8080/user/all


Buka http://localhost:8080